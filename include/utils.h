#pragma once

#include <string.h>
#include <sys/time.h>

static inline int calc_apparent_length(char *text, int bytes) {
	bool inside_tag = false;
    int len = 0;
	for (char* t = text; bytes; t++, bytes--) {
        char c = *t;
		if (c == '<')
			inside_tag = true;
		else if (c == '>')
		    inside_tag = false;
        else if (!inside_tag)
            len++;
	}
	return len;
}

static inline unsigned long millis() {
  struct timeval time;
  gettimeofday(&time, 0);
  unsigned long s1 = (unsigned long) time.tv_sec * 1000;
  unsigned long s2 = time.tv_usec / 1000;
  return s1 + s2;
}

static inline char *write_str(char *restrict dest, const char *restrict orig) {
    for (const char *o = orig; *o; o++, dest++)
        *dest = *o;
    return dest;
}