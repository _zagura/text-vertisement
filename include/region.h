#pragma once

// hint for the real type (will be ignored)
#define regptr(x) regptr

typedef unsigned int regptr;

typedef struct {
    void *address;
    regptr next;
    unsigned int capacity;
} Region;

/// @param init_capacity Can't be zero
void region_new(Region *r, unsigned int init_capacity);
void region_destroy(Region *r);
void region_reset(Region *r);

void region_shrink_to_fit(Region *r);

regptr region_alloc(Region *r, unsigned int bytes);

regptr region_append(Region *r, const Region *other);
regptr region_append_mem(Region *r, const void *src, unsigned int bytes);
regptr region_append_str(Region *r, const char *src);

/// Just returns where the next allocation will be stored
regptr region_start_alloc(const Region *r);

/// Forgets last [bytes] bytes
void region_trim(Region *r, unsigned int bytes);

void *region_get(const Region *r, regptr ptr);
void *region_get_last(const Region *r, unsigned int size);

static inline void *region_alloc_and_get(Region *r, unsigned int bytes) {
    return region_get(r, region_alloc(r, bytes));
};

#define region_alloc_and_get_type(r, type) ((type*) region_alloc_and_get(r, sizeof(type)))
#define region_alloc_type(r, type) region_alloc(r, sizeof(type))
#define region_get_type(r, ptr, type) ((type*) region_get(r, ptr))
#define region_get_last_type(r, ptr, type) ((type*) region_get_last(r, ptr))

void region_skip_to_align(Region *r, unsigned int bytes);

/// Moves everything at and after [at] by [size] bytes to the right
void region_separate(Region *r, regptr at, unsigned int bytes);