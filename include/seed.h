#pragma once

static inline long seed_next(
    unsigned long      *seed
) {
    long a = *seed ^ (*seed << 21);
    long b = a ^ (a >> 35);
    long c = b ^ (b << 4);
    *seed = c;
    return c;
}

/// @param upper_bound is exclusive
static inline long seed_next_less_than(
    unsigned long      *seed,
    unsigned long       upper_bound
) {
    return seed_next(seed) % upper_bound;
}

/// @param upper_bound is exclusive
static inline long seed_next_in_range(
    unsigned long      *seed,
    long                min,
    long                upper_bound
) {
    return min + seed_next_less_than(seed, upper_bound - min);
}

/// @returns true with a chance of 1 in our upper-bound
static inline bool seed_one_in(
    unsigned long      *seed,
    unsigned long       upper_bound
) {
    return seed_next_less_than(seed, upper_bound) == 0;
}