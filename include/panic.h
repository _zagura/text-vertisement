#pragma once

#include <stdio.h>
#include <stdlib.h>

#ifdef DEBUG
#include <execinfo.h>
#define panic_code(CODE, MESSAGE, ...) { \
    const char *message = MESSAGE; \
    printf("\n\n"); \
    fprintf(stderr, "[ERROR] "); \
    fprintf(stderr, message, ##__VA_ARGS__); \
    fprintf(stderr, "\n"); \
    void* callstack[128]; \
    int frames = backtrace(callstack, 128); \
    char** strs = backtrace_symbols(callstack, frames); \
    for (int i = 0; i < frames; ++i) \
        printf(" at %s\n", strs[i]); \
    free(strs); \
    fflush(stderr); \
    exit(CODE);}
#else
#define panic_code(CODE, MESSAGE, ...) { \
    const char *message = MESSAGE; \
    printf("\n\n"); \
    fprintf(stderr, "[ERROR] "); \
    fprintf(stderr, message, ##__VA_ARGS__); \
    fprintf(stderr, "\n"); \
    fflush(stderr); \
    exit(CODE);}
#endif

#define panic(MESSAGE, ...) panic_code(1, MESSAGE, ##__VA_ARGS__)