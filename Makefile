CFLAGS = -std=c2x -Wall -Werror -Wno-format -Wno-unused-variable -Wno-unused-but-set-variable
SOURCES = main.c region.c

release:
	cd src && clang $(SOURCES) -I../include -o ../env/text-vertisement $(CFLAGS) -O3 -s

debug:
	cd src && clang $(SOURCES) -I../include -o ../env/text-vertisement $(CFLAGS) -O0 -g -rdynamic -DDEBUG=1 -fsanitize=address,leak,undefined

test: debug
	cd env && ./text-vertisement