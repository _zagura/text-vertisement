# Text-vertisements

This is the source code for [tv.zagura.one](https://tv.zagura.one).

You may think the code is ugly, but the main objective of this is
to be as lightweight, small and simple as possible, since I don't
wanna pay more than I have to for the server.

## Usage

The executable expects there to be a `config`, and an `adverts`
file in its environment, as well as an `ascii` directory, which
can contain the ascii art.

### Config
The config file has the following syntax:
```
host tv.zagura.one
port 8081
```
If you want to run your own instance of text-vertisements, you'll
have to change the port and the host address (domain name) to
whatever you'll be using.

### Adverts
The adverts file is a list of all the simple text adverts. Empty
lines are ignored, and so are comments, which are written like this:
```lua
an advert
another advert

--- some comment (the space after the --- is obligatory)
advert again
more advert
```
Each advert must be no longer than 50 characters.

You can check if your adverts have been properly read by checking
`your.host.or.domain/adverts`.

### Ascii
Each file in the ascii folder is its own ascii art advert, it must
not exceed 40×4 characters in size.

You can use html elements in both simple text and ascii art adverts.