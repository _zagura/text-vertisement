#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <pthread.h>
#include <unistd.h>
#include <dirent.h>

#include "panic.h"
#include "region.h"
#include "seed.h"
#include "utils.h"

#define REQUEST_BUFFER_SIZE 1024
#define RESPONSE_BUFFER_SIZE 1024

#define ASCII_WIDTH (40)

const char RESPONSE_NOT_FOUND[]   = "HTTP/1.1 404 Not Found\n"   "content-type: text/plain\n\n404: Not Found";

const char MAIN_PAGE[] = "HTTP/1.1 200 OK\ncontent-type: text/html\n\n"
    "<!doctype html><meta charset=utf-8><title>Text-vertisements</title>"
    "<meta name=viewport content=\"width=device-width,initial-scale=1,shrink-to-fit=no\">"
    "<meta name=no-email-collection>"
    "<style>body{max-width:560px;padding:2em;margin:4vh auto;line-height:1.4;font-family:Cantarell,Inter,'Noto Sans',system-ui,sans-serif;color:#fffcf4;background:#1a0d22}"
    "h2,h3{font-size:1em}h3{color:#ac6087}a{color:#f046a6}"
    "h1{font-size:1.5em;font-weight:normal}"
    "h1::after{content:\"\";width:48px;height:1px;display:block;background:#583253;margin-top:1rem}"
    "details{box-sizing:border-box;width:100%;margin:1em 0;border-radius:5px;border:2px solid #52114b;background:#100616}"
    "dl{margin:0;display:grid;grid-template-columns:auto auto}"
    "dt+dd{margin:0}"
    "summary,dt,dd{padding:.3em.5em}"
    "pre{margin:0;padding:1em;white-space:pre-wrap;overflow-wrap:break-word}"
    "hr{border:none;margin:2em 0;border-top:1px dashed #ac6087}"
    "footer{margin-top:2em;color:#ac6087;border-top:1px dashed}"
    "@media(prefers-color-scheme:light){body{background:#fff8f9;color:#57131d}a{color:#ca196c}details{background:#fff;border-color:#e26ca9}}</style>"

	"<iframe src=a?s14px&cf2e4e9&l57131d&ef046a6 height=56px width=100% style=\"display:block;margin:10vh 0;border:none\"></iframe>"

	"<h1>Wellcome to text-vertising!</h1>"
    "<p>(Currently in alpha!!!)</p>"
	"<p><dfn>Text-vertisements</dfn> are small messages that can show up on websites that add them. Some are just for fun, and some promote something (non-commercial).</p>"
	"<p>There are 2 formats of text-vertisements: single-line (/l), and ascii art (/a), which is 40 by 3 characters. There's an ascii text-vertisement at the top of this site, and a single-line one at the bottom</p>"

	"<p>If you want to add a text-vertisement to your website, copy this code:</p>"
	"<details open><summary>Single-line</summary>"
	"<pre>&lt;iframe\n  src=http://tv.zagura.one/l?s14px&cfff&l000\n  height=14px width=100%\n  style=border:none;display:block>&lt;/iframe></pre></details>"
	"<details open><summary>Ascii art</summary>"
	"<pre>&lt;iframe\n  src=http://tv.zagura.one/a?s14px&cfff&l000\n  height=56px width=100%\n  style=border:none;display:block>&lt;/iframe></pre></details>"

	"<p>(Make sure you aren't using color-scheme anywhere, cuz that messes with the iframe's background)</p>"

	"<p>You can customize how text-vertisements look by changing the url's query string, "
    "which is a <code>&</code>-separated list of items, each consisting of a character, "
    "followed by the value, like so: <code>c22f&s18px</code></p>"

	"<details><summary>Customization Params</summary><dl>"
	"<dt>s</dt><dd>Text size: CSS size value</dd>"
	"<dt>c</dt><dd>Text color: CSS hex color code, but without #</dd>"
	"<dt>l</dt><dd>Text color override in light mode</dd>"
	"<dt>d</dt><dd>Text color override in dark mode</dd>"
	"<dt>e</dt><dd>Text color override for &lt;em> tags</dd>"
	"</dl></details>"

    "<hr>"
	"<h2>Submit your own</h2>"
	"<p>You can submit your own text-vertisement, be it a single-line message, or ascii art. Here are the submission rules:</p>"

	"<h3>Format</h3>"
	"<ul>"
	"<li>Only ascii characters must be used</li>"
	"<li>Ascii art must be 40x4 (also keep in mind that the bottom right 4 characters will be overriden with a link to this site)</li>"
	"<li>Single-line messages must not exceed 50 characters</li>"
	"<li>You may use simple format tags, like &lt;b>, &lt;i>, &lt;s>, &lt;em>, and links (&lt;a>)</li>"
	"<li>The total size of the text must not exceed 1KB</li>"
	"</ul>"

	"<h3>Content</h3>"
	"<ul>"
	"<li>No bigotry, obviously</li>"
	"<li>Be gay, do crime</li>"
	"<li>No promoting any commercial product, especially those owned by big companies (you can promote FOSS projects that aren't controlled by corpos)</li>"
	"</ul>"

	"<p>The admin may remove or revoke any text-vertisement at any time, regardless of the rules</p>"
	"<p>"
        "If you are capable of following these rules, send your submissions to "
        "<a href=\"mailto:me@zagura.one?subject=Text-vertisement submission\">me@zagura.one</a> "
        "or open an issue in the <a href=https://gitlab.com/_zagura/text-vertisement>source repository</a>."
    "</p>"

    "<hr>"
	"<iframe src=l?s14px&cf2e4e9&l57131d&ef046a6 height=14px width=100% style=border:none;display:block></iframe>"
    "<footer>"
        "<p>Text-vertisements were inspired by <a href=https://john.citrons.xyz>johnvertisements</a></p>"
        "<p>Implemented in C, by <a href=https://zagura.one>Zagura</a> (fedi: <a rel=me href=https://cathode.church/@zagura>@zagura@cathode.church</a>)</p>"
        "<p><a href=https://gitlab.com/_zagura/text-vertisement>See source code</a></p>"
    "</footer>";

#define DECORATIONS_COUNT 11
const char *decorations[DECORATIONS_COUNT] = { "-.", "--", "~-", "~~", "||", "\\/", "::", ":.", "_-", "xx", "**" };

int ascii_index_count;
int *ascii_indices;
char *ascii_text;

int advert_index_count;
int *advert_indices;
char *advert_text;

char *address;

int request_count_ascii = 0;
int request_count_advert = 0;

char *build_advert_top(const char *path, char **response) {
    char *text = malloc(RESPONSE_BUFFER_SIZE);
    *response = text;
    const char *s = "HTTP/1.1 200 OK\ncontent-type: text/html\n"
        "cache-control: no-cache,no-store,must-revalidate\n"
        "expires: 0\npragma: no-cache\n\n"
        "<!doctype html><base target=_parent><meta charset=utf-8>"
        "<link rel=stylesheet href=s";
    text = write_str(text, s);
    if (path[1] == '?')
        text = write_str(text, path + 1);
    *text = '>';
    return text + 1;
}

bool build_advert(const char *path, char **response, size_t *response_len) {
    request_count_advert++;
    char *text = build_advert_top(path, response);
    text = write_str(text, "<a id=tv href=https://");
    text = write_str(text, address);
    text = write_str(text, ">~tv~</a> ");

    unsigned long m = millis();
    int i = seed_next_less_than(&m, advert_index_count);
    int l = advert_indices[i + 1] - advert_indices[i];
    memcpy(text, &advert_text[advert_indices[i]], l);
    text += l;

    *response_len = text - *response;
    return true;
}

bool build_ascii(const char *path, char **response, size_t *response_len) {
    request_count_ascii++;
    char *text = build_advert_top(path, response);
    text = write_str(text, "<pre>");

    unsigned long m = millis();
    try_again:
    if (seed_one_in(&m, 5)) {
        int i = seed_next_less_than(&m, advert_index_count);
        int l = calc_apparent_length(&advert_text[advert_indices[i]], advert_indices[i + 1] - advert_indices[i]);
        if (l > ASCII_WIDTH)
            goto try_again;

        int x = (ASCII_WIDTH - l) / 2;
        const char *d = decorations[seed_next_less_than(&m, DECORATIONS_COUNT)];

        for (int i = 0; i < ASCII_WIDTH / 2; i++)
            text = write_str(text, d);
        text = write_str(text, "\n");
        
        for (int i = 0; i < x; i++)
            text = write_str(text, " ");
        memcpy(text, &advert_text[advert_indices[i]], advert_indices[i + 1] - advert_indices[i]);
        text += advert_indices[i + 1] - advert_indices[i];

        text = write_str(text, "\n");
        for (int i = 0; i < ASCII_WIDTH / 2; i++)
            text = write_str(text, d);
        text = write_str(text, "\n");
        for (int i = 0; i < ASCII_WIDTH - 4; i++)
            text = write_str(text, " ");
    } else {
        int i = seed_next_less_than(&m, ascii_index_count);
        memcpy(text, &ascii_text[ascii_indices[i]], ascii_indices[i + 1] - ascii_indices[i]);
        text += ascii_indices[i + 1] - ascii_indices[i];
    }

    text = write_str(text, "<a id=tv href=https://");
    text = write_str(text, address);
    text = write_str(text, ">~tv~</a></pre>");

    *response_len = text - *response;
    return true;
}

bool build_style(const char *path, char **response, size_t *response_len) {
    const char *color = "000";
    const char *size = "14px";
    const char *color_dark = 0;
    const char *color_light = 0;
    const char *color_em = 0;

    // Parse query string (if present)
    char buf[5][9];
    if (path[1] == '?') {
        path += 2;
        for (int i = 0; i < 5; i++) {
            char *s = buf[i];
            char c = *path;
            if (!c) break;
            path++;
            int j = 0;
            for (; j < 8 && *path && *path != '&'; j++, path++)
                s[j] = *path;
            s[j] = 0;
            switch (c) {
                case 's': size        = s; break;
                case 'c': color       = s; break;
                case 'l': color_light = s; break;
                case 'd': color_dark  = s; break;
                case 'e': color_em    = s; break;
            }
            if (!*path || *path != '&')
                break;
            path++;
        }
    }

    char *text = malloc(RESPONSE_BUFFER_SIZE);
    *response = text;
    text = write_str(text, "HTTP/1.1 200 OK\ncontent-type: text/css\n\n"
        "html{display:flex;height:100%;font-size:");
    text = write_str(text, size);
    text = write_str(text, "}pre{margin:0;font:unset}a{color:unset}#tv:not(:hover){opacity:.5;text-decoration:none}"
        "body{align-self:center;margin:0 auto;line-height:1;font-size:calc(1em - (1lh - 1cap) / 2);font-family:'JetBrains Mono',ui-monospace,\"Ubuntu Monospace\",\"Source Code Pro\",\"Fira Mono\",\"Droid Sans Mono\",monospace;color:#");
    text = write_str(text, color);
    if (color_em) {
        text = write_str(text, "}em{font:unset;color:#");
        text = write_str(text, color_em);
    }
    text = write_str(text, "}");
    if (color_dark || color_light) {
        text = write_str(text, "@media(prefers-color-scheme:");
        if (color_dark) {
            color_light = color_dark;
            text = write_str(text, "dark");
        } else {
            text = write_str(text, "light");
        }
        text = write_str(text, "){body{color:#");
        text = write_str(text, color_light);
        text = write_str(text, "}}");
    }
    *response_len = text - *response;
    return true;
}

bool build_advert_list(char **response, size_t *response_len) {
    
    const char *s = "HTTP/1.1 200 OK\ncontent-type: text/html\n\n"
        "<!doctype html><meta charset=utf-8>"
        "<meta name=viewport content=\"width=device-width,initial-scale=1,shrink-to-fit=no\">"
        "<title>All Adverts</title><h1>All Adverts</h1>";

    char *text = malloc(RESPONSE_BUFFER_SIZE * 2);
    *response = text;

    text = write_str(text, s);
    text = write_str(text, "<p>All the adverts that <a href=");
    text = write_str(text, address);
    text = write_str(text, ">text-vertisements</a> use</p>");
    text = write_str(text, "<ul>");
    for (int i = 0; i < advert_index_count; i++) {
        text = write_str(text, "<li>");

        int l = advert_indices[i + 1] - advert_indices[i];
        memcpy(text, &advert_text[advert_indices[i]], l);
        text += l;

        text = write_str(text, "</li>");
    }
    text = write_str(text, "</ul>");

    *response_len = text - *response;
    return true;
}

/// @returns whether response should be free'd
bool handle_path(const char *path, char **response, size_t *response_len) {
    if (path[0] == 0) {
        *response = (char*) MAIN_PAGE;
        *response_len = sizeof(MAIN_PAGE) - 1;
        return false;
    }

    if (path[1] == 0 || path[1] == '?') switch (path[0]) {
        case 's':
            return build_style(path, response, response_len);
        case 'a':
            return build_ascii(path, response, response_len);
        case 'l':
            return build_advert(path, response, response_len);
    }

    if (strcmp(path, "adverts") == 0)
        return build_advert_list(response, response_len);
    
    *response = (char*) RESPONSE_NOT_FOUND;
    *response_len = sizeof(RESPONSE_NOT_FOUND) - 1;
    return false;
}

void *handle_client(void *arg) {
    int client_fd = *((int*) arg);
    char *buffer = (char*) malloc(REQUEST_BUFFER_SIZE);
    ssize_t bytes_received = recv(client_fd, buffer, REQUEST_BUFFER_SIZE, 0);
    if (bytes_received > 0) {
        buffer[REQUEST_BUFFER_SIZE - 1] = 0;
        if (buffer[0] == 'G' &&
            buffer[1] == 'E' &&
            buffer[2] == 'T' &&
            buffer[3] == ' ' &&
            buffer[4] == '/') {
            int i = 5;
            while (i < REQUEST_BUFFER_SIZE && buffer[i] != ' ')
                i++;
            if (i + 6 < REQUEST_BUFFER_SIZE &&
                buffer[i + 1] == 'H' &&
                buffer[i + 2] == 'T' &&
                buffer[i + 3] == 'T' &&
                buffer[i + 4] == 'P' &&
                buffer[i + 5] == '/' &&
                buffer[i + 6] == '1') {
                buffer[i] = 0;
                const char *path = &buffer[5];
                char *response;
                size_t response_len;
                bool should_free = handle_path(path, &response, &response_len);
                send(client_fd, response, response_len, 0);
                if (should_free)
                    free(response);
            }
        }
    }
    close(client_fd);
    free(arg);
    free(buffer);
    return NULL;
}

void preload() {
    Region indices;
    Region data;

    printf("~> Loading ascii art...\n");

    region_new(&indices, 3);
    region_new(&data, ASCII_WIDTH * 4);

    DIR *ascii_dir = opendir("ascii");
    if (!ascii_dir)
        panic("Missing ascii directory");
    struct dirent *entry;
    char name_buf[128];
	while ((entry = readdir(ascii_dir))) {
        if (strcmp(entry -> d_name, ".") == 0 || strcmp(entry -> d_name, "..") == 0)
            continue;
        sprintf(name_buf, "ascii/%s", entry -> d_name);
        FILE *file = fopen(name_buf, "r");
		if (!file)
            panic("Missing file: ascii/%s", entry -> d_name);

        int *c = region_alloc_and_get(&indices, sizeof(int));
        *c = data.next;
        
        int ch;
        int x = 0, y = 0;
        while ((ch = fgetc(file)) != EOF) {
            if (ch == '\n') {
                y++;
                if (y >= 4)
                    panic("Ascii art in \"%s\" is higher than the max height (4)", entry -> d_name);
                int l = calc_apparent_length(region_get(&data, data.next - x + 1), x);
                if (l > ASCII_WIDTH)
                    panic("Line %d of ascii art in \"%s\" is longer than the max width (%u > %u)", y, entry -> d_name, l, ASCII_WIDTH);
                x = 0;
            } else x++;
            *(char*) region_alloc_and_get(&data, sizeof(char)) = ch;
        }
        fclose(file);
    }
    closedir(ascii_dir);

    ascii_index_count = indices.next / sizeof(int);
    ascii_indices = indices.address;
    ascii_text = data.address;

    int *c = region_alloc_and_get(&indices, sizeof(int));
    *c = data.next;

    printf("~> Loading adverts...\n");

    region_new(&indices, 3);
    region_new(&data, ASCII_WIDTH);

    FILE *file = fopen("adverts", "r");
    if (!file)
        panic("Missing adverts file");

    c = region_alloc_and_get(&indices, sizeof(int));
    *c = data.next;

    int line = 1;
    int ch;
    do {
        int line_len = 0;
        int comment_counter = 1;
        while ((ch = fgetc(file)) != EOF && ch != '\n') {
            if (comment_counter) {
                if (comment_counter == 4) {
                    if (ch == ' ') break;
                    else comment_counter = 0;
                } else
                    if (ch == '-') comment_counter++;
                    else comment_counter = 0;
            }
            line_len++;
            *(char*) region_alloc_and_get(&data, sizeof(char)) = ch;
        }
        // Handles comments (syntax: "--- comment")
        if (comment_counter) {
            if (ch != EOF && ch != '\n')
                while ((ch = fgetc(file)) != EOF && ch != '\n') {}
            data.next = *c;
            line++;
            continue;
        }
        // Skips empty lines
        if (!line_len) {
            line++;
            continue;
        }
        int apparent_len = calc_apparent_length(region_get(&data, *c + 1), line_len);
        if (apparent_len > 50)
            panic("Advert on line %u is longer than the max length (%u > 50)", line, apparent_len);
        c = region_alloc_and_get(&indices, sizeof(int));
        *c = data.next;
        line++;
    } while (ch != EOF);
    fclose(file);

    advert_index_count = indices.next / sizeof(int) - 1;
    advert_indices = indices.address;
    advert_text = data.address;
}

void free_preloaded() {
    free(ascii_indices);
    free(ascii_text);
}

void load_config(char *host, int *port) {
    printf("~> Reading config...\n");

    FILE *f = fopen("config", "r");
    int ch = 0;
    while (ch != EOF) {
        char buf[16];
        int x = 0;
        while ((ch = fgetc(f)) != EOF && ch != ' ' && ch != '\n') {
            buf[x++] = ch;
        }
        buf[x] = 0;
        if (strcmp(buf, "port") == 0) {
            while ((ch = fgetc(f)) != EOF && ch != '\n') {
                *port *= 10;
                *port += ch - '0';
            }
        }
        else if (strcmp(buf, "host") == 0) {
            x = 0;
            while ((ch = fgetc(f)) != EOF && ch != '\n') {
                host[x++] = ch;
            }
        }
    }
    fclose(f);

    printf("    host %s\n", host);
    printf("    port %u\n", *port);
}

int main(int argc, char **argv) {
    char host[24];
    int port;
    load_config(host, &port);

    address = host;

    preload();

    int server_fd;
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        free_preloaded();
        panic("Socket failed");
    }

    setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &(int[]) { 1 }, sizeof(int));

    struct sockaddr_in server_addr = {
        .sin_family = AF_INET,
        .sin_addr.s_addr = INADDR_ANY,
        .sin_port = htons(port),
    };

    if (bind(server_fd, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0) {
        close(server_fd);
        free_preloaded();
        panic("Bind failed");
    }

    printf("~> Setup completed: listening for requests\n");

    if (listen(server_fd, 10) < 0) {
        close(server_fd);
        free_preloaded();
        panic("Listen failed");
    }

    while (true) {
        struct sockaddr_in client_addr;
        socklen_t client_addr_len = sizeof(client_addr);
        int *client_fd = malloc(sizeof(int));

        if ((*client_fd = accept(server_fd, (struct sockaddr*) &client_addr, &client_addr_len)) < 0) {
            fprintf(stderr, "[WARN] Accept failed\n");
            continue;
        }

        pthread_t thread_id;
        pthread_create(&thread_id, NULL, handle_client, (void*) client_fd);
        pthread_detach(thread_id);
    }
}