#include <string.h>
#include <stdlib.h>

#include "region.h"
#include "panic.h"

#ifdef DEBUG
#include <stdio.h>
#endif

void region_new(Region *r, unsigned int size) {
    #ifdef DEBUG
    if (size == 0)
        panic("Reg:%x: Can't create an empty region", r);
    #endif
    r -> address = malloc(size);
    r -> next = 0;
    r -> capacity = size;
    #ifdef DEBUG
    printf("Reg:%x: Created with %u bytes\n", r, size);
    #endif
}

void region_destroy(Region *r) {
    #ifdef DEBUG
    if (r -> address == 0)
        panic("Reg:%x: Tried freeing a freed region", r);
    #endif
    free(r -> address);
    #ifdef DEBUG
    r -> address = 0;
    printf("Reg:%x: Destroyed\n", r);
    #endif
}

void region_reset(Region *r) {
    #ifdef DEBUG
    if (r -> address == 0)
        panic("Reg:%x: Tried using a freed region", r);
    #endif
    r -> next = 0;
}

void region_shrink_to_fit(Region *r) {
    #ifdef DEBUG
    if (r -> address == 0)
        panic("Reg:%x: Tried using a freed region", r);
    #endif
    if (r -> capacity == r -> next)
        return;
    r -> capacity = r -> next;
    void *p = realloc(r -> address, r -> capacity);
    if (!p) {
        printf("realloc() failed\n");
        exit(1);
    }
    r -> address = p;
}

regptr region_alloc(Region *r, unsigned int size) {
    #ifdef DEBUG
    if (r -> address == 0)
        panic("Reg:%x: Tried using a freed region", r);
    if (size == 0)
        panic("Reg:%x: Tried allocating 0 bytes", r);
    if (r -> next + size == 0)
        panic("Reg:%x: Reached wrap-around index, can't allocate any more", r);
    #endif
    regptr ptr = r -> next;
    regptr new_next = ptr + size;
    if (new_next >= r -> capacity) {
        while (new_next >= r -> capacity)
            r -> capacity *= 2;
        void *p = realloc(r -> address, r -> capacity);
        if (!p) {
            printf("realloc() failed\n");
            exit(1);
        }
        r -> address = p;
    }
    r -> next = new_next;
    return ptr + 1;
}

regptr region_append(Region *r, const Region *other) {
    regptr p = region_alloc(r, other -> next);
    memcpy(region_get(r, p), other -> address, other -> next);
    return p;
}

regptr region_append_mem(Region *r, const void *src, unsigned int bytes) {
    regptr p = region_alloc(r, bytes);
    memcpy(region_get(r, p), src, bytes);
    return p;
}

regptr region_append_str(Region *r, const char *src) {
    regptr p = region_alloc(r, strlen(src));
    strcpy(region_get(r, p), src);
    return p;
}

regptr region_start_alloc(const Region *r) {
    #ifdef DEBUG
    if (r -> address == 0)
        panic("Reg:%x: Tried using a freed region", r);
    #endif
    return r -> next + 1;
}

void region_trim(Region *r, unsigned int bytes) {
    #ifdef DEBUG
    if (r -> address == 0)
        panic("Reg:%x: Tried using a freed region", r);
    #endif
    r -> next -= bytes;
}

void *region_get(const Region *r, regptr ptr) {
    #ifdef DEBUG
    if (r -> address == 0)
        panic("Reg:%x: Tried using a freed region", r);
    if (ptr == 0)
        panic("Reg:%x: Tried accessing a null pointer", r);
    if (ptr > r -> next)
        panic("Reg:%x: Tried accessing a regptr out of region's bounds (ptr %i > len %i)", r, ptr, r -> next);
    
    // Try to read from address
    char _ = *(char*) (r -> address + ptr - 1);
    #endif
    return (void*) (r -> address + ptr - 1);
}

void *region_get_last(const Region *r, unsigned int size) {
    #ifdef DEBUG
    if (r -> address == 0)
        panic("Reg:%x: Tried using a freed region", r);
    #endif
    return (void*) (r -> address + r -> next - size);
}


void region_skip_to_align(Region *r, unsigned int bytes) {
    int over = ((long) r -> address + r -> next) % bytes;
    if (over)
        region_alloc(r, bytes - over);
}

void region_separate(Region *r, regptr at, unsigned int bytes) {
    region_alloc(r, bytes);
    void *addr = region_get(r, at);
    memmove(addr + bytes, addr, r -> next - at + 1);
}